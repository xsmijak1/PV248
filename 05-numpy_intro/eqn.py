import sys
import numpy as np
import re

file_name =  sys.argv[1]
eq_coef_dicts = []
eq_constants_list = []

with open(file_name, 'r') as handle:
    for line in handle:
        if '=' not in line:
            continue

        sides = line.split(' = ')

        left_side = re.sub("-\s*", "-", sides[0])
        left_side = re.sub("\+\s*", "+", left_side)
        left_side_tokens = left_side.split(" ")

        coef_dict = {}
        for token in left_side_tokens:
            if len(token) == 0:
                pass
            elif any(char.isdigit() for char in token):
                coef_dict[token[-1]] = int(token[:-1])
            elif len(token) == 1 or token[0] == "+":
                coef_dict[token[-1]] = 1
            else:
                coef_dict[token[-1]] = -1
        if len(coef_dict) != 0:
            eq_coef_dicts.append(coef_dict)

            eq_const = int(sides[1].strip())
            eq_constants_list.append(eq_const)

sets_of_unknowns = [set(e.keys()) for e in eq_coef_dicts]
all_unknowns = sorted((set.union(*sets_of_unknowns)))

eq_coef_lists = []
for coef_dict in eq_coef_dicts:
    coef_list = []
    for v in all_unknowns:
        val = coef_dict.get(v, 0)
        coef_list.append(val)
    eq_coef_lists.append(coef_list)

matrix = np.array(eq_coef_lists)
vector = np.array(eq_constants_list)
augmented_matrix = np.array([v + [eq_constants_list[i]] for i, v in enumerate(eq_coef_lists)])

coef_rank = np.linalg.matrix_rank(matrix)
aug_rank = np.linalg.matrix_rank(augmented_matrix)
rank_diff = aug_rank - coef_rank
n_diff = len(all_unknowns) - coef_rank

if rank_diff != 0:
    print("no solution")
elif n_diff == 0:
    x = np.linalg.solve(matrix, vector)
    solutions = ", ".join([str(all_unknowns[i]) + " = " + str(v) for i, v in enumerate(x)])
    print("solution: " + solutions)
else:
    print("solution space dimension: " + str(n_diff))
