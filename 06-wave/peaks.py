import wave
import sys
import struct
import numpy

file_name = sys.argv[1]
x = wave.open(file_name, 'rb')

is_stereo = x.getnchannels() == 2
frame_num = x.getnframes()
frequency = x.getframerate()

frame = x.readframes(1)
signals = []
while frame:
    if is_stereo:
        unpacked_frame = struct.unpack('<hh', frame)
        signal = (unpacked_frame[0] + unpacked_frame[1]) / 2.0
    else:
        unpacked_frame = struct.unpack('<h', frame)
        signal = unpacked_frame[0] * 1.0

    signals.append(signal)
    frame = x.readframes(1)

x.close()

window_size = frequency
windows_num = frame_num // window_size

min = None
max = None

for i in range(windows_num):
    lower_ix = i*window_size
    upper_ix = (i+1)*window_size
    amplitudes = [ numpy.abs(a) for a in numpy.fft.rfft(signals[lower_ix:upper_ix]) ]
    avg = numpy.average(amplitudes)
    interesting_avgs = [ i for i, a in enumerate(amplitudes) if a >= 20*avg ]
    for a in interesting_avgs:
        if min is None or a < min:
            min = a
        if max is None or a > max:
            max = a

if min is None:
    print("no peaks")
else:
    print("low = %d, high = %d" % (min, max))
