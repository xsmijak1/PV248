import http.server
import json
import socketserver
import sys


class Game:
    def __init__(self, name):
        self.name = name
        self.current_player = 1
        self.board = [[0, 0, 0],
                      [0, 0, 0],
                      [0, 0, 0]]
        self.winner = None


    def get_next_player(self):
        return 1 if self.current_player == 2 else 2


    def get_status(self):
        if self.winner is not None:
            return {'winner': self.winner, 'board': self.board}
        return {'board': self.board, 'next': self.current_player}


    def play(self, x, y, player):
        if self.winner != None:
            return {"status": "bad", "message": "Game is already over"}

        if player != self.current_player:
            return {"status": "bad", "message": "It's not a turn of the player"}

        if x > 2 or x < 0:
            return {"status": "bad", "message": "X out of bounds"}

        if y > 2 or y < 0:
            return {"status": "bad", "message": "Y out of bounds"}

        if self.board[y][x] != 0:
            return {"status": "bad", "message": "Field is not empty"}

        self.board[y][x] = self.current_player
        self.current_player = self.get_next_player()
        self.eval_winner()
        return {"status": "ok"}

    
    def eval_winner(self):
        if self.winner != None:
            return

        if self.board[0][0] != 0 and self.board[0][0] == self.board[1][1] and self.board[1][1] == self.board[2][2]:
            self.winner = self.board[0][0]
            return
        if self.board[0][2] != 0 and self.board[0][2] == self.board[1][1] and self.board[1][1] == self.board[2][0]:
            self.winner = self.board[0][2]
            return

        for i in range(3):
            if self.board[0][i] != 0 and self.board[0][i] == self.board[1][i] and self.board[1][i] == self.board[2][i]:
                self.winner = self.board[0][i]
                return
            if self.board[i][0] != 0 and self.board[i][0] == self.board[i][1] and self.board[i][1] == self.board[i][2]:
                self.winner = self.board[i][0]
                return

        if all([all(i != 0 for i in row) for row in self.board]):
           self.winner = 0
           return


# input: part of url after '?'
def get_url_params(params):
    parameters_parts = params.split('&')
    pars = {}
    for p in parameters_parts:
        pts = p.split('=')
        if len(pts) < 2:
            pars[pts[0]] = None
        else:
            pars[pts[0]] = pts[1]
    return pars


def get_number_from_params(params, param_name):
    param_val = params.get(param_name)
    if param_val == None or (not str.isdigit(param_val)):
        return None
    return int(param_val)


class Handler(http.server.SimpleHTTPRequestHandler):
    games = []

    def respond(self, code=200, response=None):
        self.send_response(code)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Connection', 'close')
        if response is not None:
            self.send_header('Content-Length', len(str(response)))
        self.end_headers()
        if response is not None:
            encoded_response = bytes(json.dumps(response, ensure_ascii=False), 'utf-8')
            self.wfile.write(encoded_response)


    def do_GET(self):
        url_parts = self.path.strip("/").split("?")
        request = url_parts[0]
        if len(url_parts) == 1:
            params = {}
        else:
            params = get_url_params(url_parts[1])

        if request == "start":
            name = params.get("name", "")
            id = len(self.games)
            game = Game(name)
            self.games.append(game)
            self.respond(response={'id': id})
            return

        if request == "status":
            game_id = get_number_from_params(params, "game")
            if game_id is None or game_id < 0 or game_id >= len(self.games):
                self.respond(code=400)
                return

            status = self.games[int(game_id)].get_status()
            self.respond(response=status)
            return

        if request == "play":
            game_id = get_number_from_params(params, "game")
            x = get_number_from_params(params, "x")
            y = get_number_from_params(params, "y")
            player = get_number_from_params(params, "player")    

            if game_id is None or game_id < 0 or game_id >= len(self.games) or x is None or y is None or player is None:
                self.respond(code=400)
                return

            status = self.games[game_id].play(x, y, player)
            self.respond(response=status)
            return

        if request == "list":
            list = [ {'id': i, 'name': game.name} for i, game in enumerate(self.games) ]
            self.respond(response=list)
            return

        self.respond(code=404)
        return


port = int(sys.argv[1])
with socketserver.TCPServer(("", port), Handler) as httpd:
    httpd.serve_forever()
