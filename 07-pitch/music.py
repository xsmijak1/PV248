import wave
import sys
import struct
import numpy

file_name = sys.argv[2]
base_a = float(sys.argv[1])
const_a = 2 ** (1.0/12)
eps = 0.001

def get_base_c_from_a(a_freq):
    return (base_a / 4.0) * (const_a ** 3)

base_c = get_base_c_from_a(base_a)

def get_base_of_tone(base, freq):
    if freq >= base and freq < 2*base:
        return base
    if freq > 2*base:
        return get_base_of_tone(2 * base, freq)
    else:
        return get_base_of_tone(base / 2.0, freq)

def get_octave_of_tone(base, freq):
    if freq >= base and freq < 2*base:
        return 0
    if freq > 2*base:
        return 1 + get_octave_of_tone(2 * base, freq)
    else:
        return -1 + get_octave_of_tone(base / 2.0, freq)

def get_tone_from_freq(freq):
    base = get_base_of_tone(base_c, freq)
    octave = get_octave_of_tone(base_c, freq)

    tone = base
    for i in range(12):
        if (abs(tone - freq)) < eps:
            return tone_to_str(octave, i, 0)

        next_tone = tone*const_a
        interval_cent = (next_tone - tone) / 100.0
        interval_half = tone + 50*interval_cent

        if freq >= tone and freq < interval_half:
            cents = (freq - tone) / interval_cent
            return tone_to_str(octave, i, int(round(cents)))
        elif freq >= interval_half and freq < (next_tone - eps):
            cents = (next_tone - freq) / interval_cent
            return tone_to_str(octave, i+1, -1 * int(round(cents)))

        tone = next_tone

tone_names = "c, cis, d, es, e, f, fis, g, gis, a, bes, b".split(", ")
def tone_to_str(octave, tone_ix, cents):
    if (tone_ix == 12):
        tone_ix = 0
        octave += 1
    tone = tone_names[tone_ix]

    if octave < 0:
        tone = tone.capitalize()
        for i in range(abs(octave)-1):
            tone += ","
    else:
        for i in range(octave):
            tone += "'"

    if cents < 0:
        tone += str(cents)
    else:
        tone += "+" + str(cents)

    return tone

x = wave.open(file_name, 'rb')

is_stereo = x.getnchannels() == 2
frame_num = x.getnframes()
frequency = x.getframerate()

frame = x.readframes(1)
signals = []
while frame:
    if is_stereo:
        unpacked_frame = struct.unpack('<hh', frame)
        signal = (unpacked_frame[0] + unpacked_frame[1]) / 2.0
    else:
        unpacked_frame = struct.unpack('<h', frame)
        signal = unpacked_frame[0] * 1.0

    signals.append(signal)
    frame = x.readframes(1)

x.close()

window_size = frequency
if frame_num % frequency == 0:
    frame_num -= frequency
windows_num = 10 * (frame_num // frequency)
jump = frequency // 10

segments = []

for window_start in range(windows_num):
    lower_ix = window_start*jump
    upper_ix = lower_ix + window_size
    amplitudes = [ numpy.abs(a) for a in numpy.fft.rfft(signals[lower_ix:upper_ix]) ]
    avg = numpy.average(amplitudes)
    peaky_amplitude_ixes = [ i for i, a in enumerate(amplitudes) if a >= 20*avg ]

    tones = []
    while len(tones) < 3 and len(peaky_amplitude_ixes) > 0:
        max_ix = -1
        max = -1
        for ix in peaky_amplitude_ixes:
            if amplitudes[ix] > max:
                max = amplitudes[ix]
                max_ix = ix

        for i in range(max_ix, -1, -1):
            if i in peaky_amplitude_ixes:
                peaky_amplitude_ixes.remove(i)
            else:
                break

        for i in range(max_ix+1, len(amplitudes)):
            if i in peaky_amplitude_ixes:
                peaky_amplitude_ixes.remove(i)
            else:
                break

        tone = get_tone_from_freq(max_ix)
        tones.append((tone, max_ix))

    if len(tones) > 0:
        tones.sort(key=lambda tone: tone[1])
        tns = [t[0] for t in tones]
        if len(segments) > 0 and segments[-1]['tones'] == tns:
            segments[-1]['end'] += 1
        else:
            segment = {
                'begin': window_start,
                'end': window_start + 1,
                'tones': tns
            }
            segments.append(segment)

for segment in segments:
    print("%02d.%d-%02d.%d %s" % (segment['begin'] // 10, segment['begin'] % 10, segment['end'] // 10, segment['end'] % 10, " ".join(segment['tones'])))
