import json
import sys
import http.server
import socketserver
import urllib.request
from urllib.error import HTTPError
import socket
import urllib.parse

port = int(sys.argv[1])
upstream = sys.argv[2].strip().rstrip("/")
if not upstream.startswith("http://") and not upstream.startswith("https://"):
    upstream = "http://" + upstream

def get_json(my_json):
  try:
    js = json.loads(my_json)
  except ValueError:
    return None
  return js

class Handler(http.server.SimpleHTTPRequestHandler):

    def do_GET(self):
        request_path = self.path
        if len(request_path) > 0 and request_path[0] != "/":
            request_path = "/" + request_path

        headers = self.headers
        url = upstream + request_path

        req = urllib.request.Request(url)
        for h in headers:
            req.add_header(h, headers[h])

        my_response = {}
        try:
            response = urllib.request.urlopen(req, timeout=1)
        except HTTPError as err:
            my_response['code'] = err.code
            my_response['headers'] = dict(err.headers.items())
        except socket.timeout:
            my_response['code'] = 'timeout'
        else:
            my_response['code'] = response.code
            my_response['headers'] = dict(response.headers.items())
            response_data = bytes.decode(response.read(), 'utf-8')
            if get_json(response_data) is not None:
                my_response['json'] = response_data
            else:
                my_response['content'] = response_data

        self.send_header( 'Connection', 'close')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(my_response, ensure_ascii=False), 'utf-8'))
        self.send_response( 200, 'OK')
        return

    def do_POST(self):
        headers = self.headers
        content_length = headers['Content-length']
        length = int(content_length) if content_length else 0
        data = self.rfile.read(length)

        js = get_json(data)
        my_response = {}
        if js is None or 'url' not in js or ('content' not in js and 'type' in js and js['type'] == 'POST'):
            my_response['code'] = 'invalid json'
            self.send_header('Connection', 'close')
            self.end_headers()
            self.wfile.write(bytes(json.dumps(my_response, ensure_ascii=False), 'utf-8'))
            self.send_response(200, 'OK')
            return

        url = js['url']

        rtimeout = int(js['timeout']) if 'timeout' in js else 1
        rtype = str(js['type']) if 'type' in js else 'GET'
        data = str(js['content']).encode('UTF-8') if 'content' in js and rtype == 'POST' else None
        headers = dict(js['headers']) if 'headers' in js else {}

        req = urllib.request.Request(url, data=data, method=rtype)
        for h in headers:
            req.add_header(h, headers[h])

        try:
            response = urllib.request.urlopen(req, timeout=rtimeout)
        except HTTPError as err:
            my_response['code'] = err.code
            my_response['headers'] = dict(err.headers.items())
        except socket.timeout:
            my_response['code'] = 'timeout'
        else:
            my_response['code'] = response.code
            my_response['headers'] = dict(response.headers.items())
            response_data = bytes.decode(response.read(), 'utf-8')
            if get_json(response_data) is not False:
                my_response['json'] = response_data
            else:
                my_response['content'] = response_data

        self.send_header('Connection', 'close')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(my_response, ensure_ascii=False), 'utf-8'))
        self.send_response(200, 'OK')


with socketserver.TCPServer(("", port), Handler) as httpd:
    httpd.serve_forever()
