import sys
import urllib
import urllib.request
import json
import time
import re


port = sys.argv[2]
upstream = sys.argv[1].strip().rstrip("/")
if not upstream.startswith("http://") and not upstream.startswith("https://"):
    upstream = "http://" + upstream
upstream = upstream + ":" + port

def is_game_available(game_data):
    if game_data is None:
        return False
    return all([all(i == 0 for i in row) for row in game_data])


def get_game_data(game_id):
    request = urllib.request.Request(upstream + '/status?game=' + str(game_id))
    response = urllib.request.urlopen(request)
    return json.load(response)


def print_available_games():
    request = urllib.request.Request(upstream + '/list')
    response = urllib.request.urlopen(request)
    all_games = json.load(response)
    available_games = []
    for game in all_games:
        game_data = get_game_data(game['id'])
        if is_game_available(game_data.get('board')):
            available_games.append(game)

    for game in available_games:
        game_id = str(game['id']).ljust(5)
        game_name = str(game['name'])
        print(' {} {}'.format(game_id, game_name))


def select_game(user_input):
    user_input = user_input.strip()
    if len(user_input) < 3 and user_input[:3] != "new":
        if not str.isdigit(user_input):
            return None

    if user_input[:3] == "new":
        name = user_input[3:].strip()
        request = urllib.request.Request(upstream + '/start?name=' + name)
        response = urllib.request.urlopen(request)
        game_data = json.load(response)
        return (game_data['id'], 1)

    game_id = int(user_input)
    game_data = get_game_data(game_id)
    if game_data is None or not is_game_available(game_data.get('board')):
        return None

    return (game_id, 2)


def is_others_players_turn(game_id, player):
    game_data = get_game_data(game_id)
    return game_data.get('next') != player and game_data.get('next') != None


def wait_for_players_turn(game_id, player):
    time.sleep(1)
    while is_others_players_turn(game_id, player):
        time.sleep(1)


def get_players_mark(i):
    if i == 1:
        return 'x'
    if i == 2:
        return 'o'
    return '_'


def print_board(game_id):
    game_data = get_game_data(game_id)
    for row in game_data['board']:
        printable_row = ''.join([get_players_mark(i) for i in row])
        print(printable_row)


def is_game_over(game_id, player, silent=False):
    game_data = get_game_data(game_id)
    winner = game_data.get('winner')
    if winner is None:
        return False
    if not silent:
        if winner == 0:
            print('draw')
        elif winner == player:
            print('you win')
        else:
            print('you lose')
    return True

def play(game_id, x, y, player):
    request = urllib.request.Request(upstream + '/play?game={}&x={}&y={}&player={}'.format(game_id, x, y, player))
    response = urllib.request.urlopen(request)
    if response.getcode() != 200:
        return False
    stats = json.load(response)
    if stats['status'] == 'ok':
        return True
    else:
        return False


def main():
    print_available_games()
    user_input = input("Choose a game from the list above or create a new one: ")
    g = select_game(user_input)
    if g is None:
        print("Invalid input, shutting down")
        return
    game_id, player = g
    mark = get_players_mark(player)
    print_board(game_id)
    while True:
        if is_game_over(game_id, player):
            break

        if is_others_players_turn(game_id, player):
            print('waiting for the other player')
            wait_for_players_turn(game_id, player)
            print_board(game_id)

        if is_game_over(game_id, player):
            break

        while True:
            user_input = input("your turn ({}): ".format(mark)).strip()
            coordinates = re.split('\s+', user_input)
            if len(coordinates) != 2 or not str.isdigit(coordinates[0]) or not str.isdigit(coordinates[1]):
                print('invalid input')
                continue
            x = int(coordinates[0])
            y = int(coordinates[1])
            if not play(game_id, x, y, player):
                print('invalid input')
            else:
                print_board(game_id)
                break

main()
