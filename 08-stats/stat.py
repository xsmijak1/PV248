import sys
import pandas
import json


file_name = sys.argv[1]
mode = sys.argv[2]

table = pandas.read_csv(file_name, header=0, index_col='student')

if mode == "dates":
    table = table.rename(lambda col: col.strip()[:10], axis='columns')
elif mode == "exercises":
    table = table.rename(lambda col: col.strip()[-2:], axis='columns')
else:
    table = table.rename(lambda col: col.strip(), axis='columns')

table = table.groupby(level=0, axis=1).sum()

results = {}
for column_name in table:
    col_results = {}
    column = table[column_name]
    col_results["mean"] = column.mean()
    col_results["median"] = column.median()
    col_results["first"] = column.quantile(0.25)
    col_results["last"] = column.quantile(0.75)
    col_results["passed"] = len([x for x in column if x > 0 ])
    results[column_name] = col_results

print(json.dumps(results, indent=2))
