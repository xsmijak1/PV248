import sys
import pandas
import json
import numpy
from datetime import datetime
from math import floor
from scipy.optimize import curve_fit

file_name = sys.argv[1]
id = sys.argv[2]

table = pandas.read_csv(file_name, header=0, index_col='student')
table = table.rename(lambda col: col.strip()[-2:], axis='columns')
table = table.groupby(level=0, axis=1).sum()

if id == 'average':
    grades = []
    for column_name in table:
        avg = table[column_name].mean()
        grades.append(avg)
else:
    grades = table.loc[int(id)].values.tolist()

results = {}
results["mean"] = numpy.mean(grades)
results["median"] = numpy.median(grades)
results["passed"] = numpy.count_nonzero(grades)
results["total"] = numpy.sum(grades)

table = pandas.read_csv(file_name, header=0, index_col='student')
table = table.rename(lambda col: col.strip()[:10], axis='columns')
table = table.groupby(level=0, axis=1).sum()
if id == 'average':
    grades = []
    for column_name in table:
        avg = table[column_name].mean()
        grades.append(avg)
else:
    grades = table.loc[int(id)].values.tolist()

ord_dates = list(map(datetime.toordinal, [pandas.datetime.strptime(d, '%Y-%m-%d') for d in table.columns.values]))

minimum_date = datetime.toordinal(pandas.datetime.strptime('2018-9-17', '%Y-%m-%d'))
zipped = zip(ord_dates, grades)
ordered = sorted(zipped, key=lambda x: x[0])
ord_dates = [x[0] - minimum_date for x in ordered]
grades = [x[1] for x in ordered]

cumulative_grades = numpy.cumsum(grades)

slope = curve_fit(lambda x, y: x * y, ord_dates, cumulative_grades)[0][0]

results["regression slope"] = slope

if slope > 0:
    sixtn_val = floor(16 / slope) + minimum_date
    results["date 16"] = datetime.fromordinal(sixtn_val).strftime("%Y-%m-%d")

    twenty_val = floor(20 / slope) + minimum_date
    results["date 20"] = datetime.fromordinal(twenty_val).strftime("%Y-%m-%d")

print(json.dumps(results, indent=2))

