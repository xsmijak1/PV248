import sys
import sqlite3
import json


def assign_if_present(json, attribute, value):
    if value is not None:
        json[attribute] = value


def voice_to_json(score_id, cur):
    cur.execute('''SELECT voice.name/*0*/, voice.range/*1*/
                   FROM voice JOIN score ON voice.score=score.id
                   WHERE score.id=?
                   ORDER BY voice.number''', (score_id,))

    voices = cur.fetchall()
    voice_jsons = []
    for v in voices:
        vj = {}
        assign_if_present(vj, "name", v[0])
        assign_if_present(vj, "range", v[1])
        voice_jsons.append(vj)

    return voice_jsons


def editor_to_json(edition_id, cur):
    cur.execute('''SELECT person.name/*0*/, person.born/*1*/, person.died/*2*/
                   FROM person JOIN edition_author ON person.id=edition_author.editor
                   WHERE edition_author.edition=?''', (edition_id,))
    editors = cur.fetchall()
    editor_jsons = []
    for e in editors:
        ej = {}
        assign_if_present(ej, "name", e[0])
        assign_if_present(ej, "born", e[1])
        assign_if_present(ej, "died", e[2])
        editor_jsons.append(ej)

    return editor_jsons


def composer_to_json(score_id, cur):
    cur.execute('''SELECT person.name/*0*/, person.born/*1*/, person.died/*2*/
                   FROM person JOIN score_author ON person.id=score_author.composer
                   WHERE score_author.score=?''', (score_id,))
    composers = cur.fetchall()
    composer_jsons = []
    for c in composers:
        cj = {}
        assign_if_present(cj, "name", c[0])
        assign_if_present(cj, "born", c[1])
        assign_if_present(cj, "died", c[2])
        composer_jsons.append(cj)

    return composer_jsons


def print_to_json(print_id, cur):
    cur.execute('''SELECT print.partiture/*0*/, print.edition/*1*/, 
                          edition.score/*2*/, edition.name/*3*/, edition.year/*4*/, edition.score/*5*/,
                          score.name/*6*/, score.genre/*7*/, score.key/*8*/, score.incipit/*9*/, score.year/*10*/                      
                   FROM print JOIN edition ON print.edition=edition.id
                              JOIN score ON edition.score=score.id
                   WHERE print.id=?''', (print_id,))
    prnt = cur.fetchall()[0]

    print_json = {}
    print_json["Print Number"] = print_id
    assign_if_present(print_json, "Composer", composer_to_json(prnt[2], cur))
    assign_if_present(print_json, "Title", prnt[6])
    assign_if_present(print_json, "Genre", prnt[7])
    assign_if_present(print_json, "Key", prnt[8])
    assign_if_present(print_json, "Composition Year", prnt[10])
    assign_if_present(print_json, "Publication Year", prnt[4])
    assign_if_present(print_json, "Edition", prnt[3])
    assign_if_present(print_json, "Editor", editor_to_json(prnt[1], cur))
    assign_if_present(print_json, "Voices", voice_to_json(prnt[2], cur))
    assign_if_present(print_json, "Partiture", prnt[0] == "Y")
    assign_if_present(print_json, "Incipit", prnt[9])

    return print_json


composer_name = sys.argv[1]
db_file = "scorelib.dat"

try:
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()

    cur.execute('''SELECT DISTINCT name FROM person WHERE person.name LIKE ?''', ("%" + composer_name + "%",))

    composers_json = {}
    for c in cur.fetchall():
        cn = c[0]
        cur.execute('''SELECT print.id 
                       FROM print JOIN edition ON print.edition=edition.id 
                                  JOIN score_author ON edition.score=score_author.score 
                                  JOIN person ON score_author.composer=person.id
                       WHERE person.name LIKE ?''', (cn,))
        prints = cur.fetchall()
        print_jsons = []
        for p in prints:
            print_jsons.append(print_to_json(p[0], cur))

        if len(print_jsons) > 0:
            composers_json[cn] = print_jsons

    print(json.dumps(composers_json, ensure_ascii=False, indent=2))

except sqlite3.Error as e:
    print(e)
finally:
    conn.close()
