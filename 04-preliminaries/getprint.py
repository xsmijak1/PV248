import sys
import sqlite3
import json

print_num = sys.argv[1]
db_file = "scorelib.dat"

try:
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()
    cur.execute('''SELECT person.name, person.born, person.died 
                   FROM print JOIN edition ON print.edition=edition.id 
                              JOIN score_author ON edition.score=score_author.score 
                              JOIN person ON score_author.composer=person.id
                   WHERE print.id=?''', (print_num,)) 
    composers = []
    for db_composer in cur.fetchall():
        composer = {}
        if db_composer[0] is not None:
            composer["name"] = db_composer[0]
        if db_composer[1] is not None:
            composer["born"] = db_composer[1]
        if db_composer[2] is not None:
            composer["died"] = db_composer[2]
        composers.append(composer)
   
    print(json.dumps(composers, ensure_ascii=False, indent=2))

except sqlite3.Error as e:
    print(e)
finally:
    conn.close()


