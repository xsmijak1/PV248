import re
import sys
from collections import Counter

args = sys.argv
file_name = args[1]
command = args[2]

def make_composer_stats(print_func):
    composer_pattern = re.compile(r"(\s*([^\d;]+[^\s(\d;])\s*\(?\d{0,4}-{0,2}\d{0,4}\)?\s*;?)+")
    init_pattern = re.compile(r"Composer: (.*)")
    counter = Counter()
    for line in open(file_name, 'r', encoding="utf8"):
        init_match = re.match(init_pattern, line)
        if init_match is None:
            continue
        match = re.match(composer_pattern, init_match.group(1))
        if match is not None:
            if len(match.groups()) > 1:
                groups = match.groups()[1:]
            else:
                groups = [match.group(1)]
            for group in groups:
                counter[group] += 1
    for key, value in counter.items():
        print_func(key, value)

def make_stats(pattern, print_func):
    counter = Counter()
    for line in open(file_name, 'r', encoding="utf8"):
        match = re.match(pattern, line)
        if match is not None:
            if len(match.groups()) > 1:
                groups = match.groups()[1:]
            else:
                groups = [match.group(1)]
            for group in groups:
                counter[group] += 1
    if "17" in counter.keys():
        counter["16"] += 1
    for key, value in counter.items():
        print_func(key, value)      

def composer_print(key, value):
    print('{}: {}'.format(key, value))

def century_print(key, value):
    print('{}th century: {}'.format(str(int(key) + 1), value))

def key_print(key, value):
    print('{}-key: {}'.format(key, value))


def main():
    if command == 'composer':
        make_composer_stats(composer_print)
        return
    elif command == 'century':
        pattern = re.compile(r"Composition Year: [^\d]*\d{0,2}.?\s*\d{0,2}.?\s*(\d{2})\d{2}")
        print_func = century_print
    elif command == 'key':
        pattern = re.compile(r"Key: (\w)")
        print_func = key_print

    make_stats(pattern, print_func)

main()

