import sys
import os
import sqlite3
from sqlite3 import Error
import scorelib


def insert_into_table(table, columns, values, cur):
    cur.execute("INSERT INTO " + table + " " + str(columns) + " VALUES (" + ", ".join(["?" for _ in range(len(columns))]) + ")", values)
    return cur.lastrowid


def store_print(prnt, cur):
    if prnt.partiture:
        partiture = "Y"
    else:
        partiture = "N"

    edition_id = store_edition(prnt.edition, cur)

    insert_into_table("print", ("id", "partiture", "edition"), (prnt.print_id, partiture, edition_id), cur)


def store_person(person, connecting_table, table_columns, entry_id, cur):
    if person is None:
        return
    query = "SELECT id, born, died FROM person WHERE name LIKE \"" + person.name + "\""
    cur.execute(query)
    existing_persons = cur.fetchall()
    if len(existing_persons) == 0:
        person_id = insert_into_table("person", ("name", "born", "died"), (person.name, person.born, person.died), cur)
    else:
        person_id = existing_persons[0][0]
        if person.born is not None:
            born = person.born
        else:
            born = existing_persons[0][1]
        if person.died is not None:
            died = person.died
        else:
            died = existing_persons[0][2]
        cur.execute("UPDATE person SET born=(?), died=(?) WHERE id=(?)", (born, died, person_id))

    insert_into_table(connecting_table, table_columns, (entry_id, person_id), cur)
    return person_id 


def store_edition(edition, cur):
    if edition is None:
        edition_name = None
        edition_score = None
    else:
        edition_name = edition.name
        edition_score = edition.composition

    score_id = store_score(edition_score, cur)
    select_params = []
    
    if edition_name is None:
        query = "SELECT id FROM edition WHERE name IS NULL AND score=?"
    else:
        query = "SELECT id FROM edition WHERE name LIKE ? AND score=?"
        select_params.append(edition_name)
    select_params.append(score_id)

    cur.execute(query, tuple(select_params))
    existing_editions = cur.fetchall()
    for current_edition in existing_editions:        
        current_edition_id = current_edition[0]
        query = "SELECT person.name FROM person INNER JOIN edition_author ON person.id=edition_author.editor WHERE edition_author.edition=" + str(current_edition_id)
        cur.execute(query)
        current_edition_editors = set(map(lambda editor: editor[0], cur.fetchall()))
        edition_authors = set(map(lambda author: author.name, edition.authors))
        if current_edition_editors == edition_authors:
            return current_edition_id

    edition_id = insert_into_table("edition", ("score", "name"), (score_id, edition_name), cur)
    
    for author in edition.authors:
        store_person(author, 'edition_author', ("edition", "editor"), edition_id, cur)

    return edition_id

def store_score(score, cur):
    if score is None:
        params = (None, None, None, None, None)
        score_voices = []
    else:
        params = (score.name, score.genre, score.key, score.incipit, score.year)
        score_voices = score.voices
    
    query = "SELECT id FROM score WHERE"
    select_params = []
    if score is None or score.name is None or "\"" in score.name:
        query += " name IS NULL"
    else:
        query += " name LIKE ?"
        select_params.append(score.name)

    if score is None or score.genre is None:
        query += " AND genre IS NULL"
    else:
        query += " AND genre LIKE ?"
        select_params.append(score.genre)

    if score is None or score.key is None:
        query += " AND key IS NULL"
    else:
        query += " AND key LIKE ?"
        select_params.append(score.key)

    if score is None or score.incipit is None:
        query += " AND incipit IS NULL"
    else:
        query += " AND incipit LIKE ?"
        select_params.append(score.incipit)

    if score is None or score.year is None:
        query += " AND year IS NULL"
    else:
        query += " AND year=?"
        select_params.append(score.year)

    cur.execute(query, tuple(select_params))
    existing_scores = cur.fetchall()
    for current_score in existing_scores:
        current_score_id = current_score[0]
        if current_score_id is None:
            continue
        query = "SELECT person.name FROM person INNER JOIN score_author ON person.id=score_author.composer WHERE score_author.score=" + str(current_score_id)
        cur.execute(query)
        current_score_authors = set(map(lambda composer: composer[0], cur.fetchall()))
        if current_score_authors != set(map(lambda author: author.name, score.authors)):
            continue
        query = "SELECT number, range, name FROM voice WHERE score=" + str(current_score_id)
        cur.execute(query)
        current_score_voices = sorted(cur.fetchall(), key=lambda voice: voice[0])
        if len(current_score_voices) != len(score_voices):
            continue

        flag = False
        for i in range(len(score_voices)):
            if score_voices[i] is None:
                if current_score_voices[i][1] is not None or current_score_voices[i][2] is not None:
                    flag = True
            elif current_score_voices[i][1] != score_voices[i].range or current_score_voices[i][2] != score_voices[i].name:
                flag = True
        if flag:
            continue

        return current_score_id
 
    score_id = insert_into_table("score", ("name", "genre", "key", "incipit", "year"), params, cur)

    if len(score_voices) > 0:
        for i in range(len(score_voices)):
            voice = score_voices[i]
            if voice is None:
                params = (i + 1, score_id, None, None)
            else:
                params = (i + 1, score_id, voice.range, voice.name)
            insert_into_table("voice", ("number", "score", "range", "name"), params, cur)

    for author in score.authors:
        store_person(author, 'score_author', ("score", "composer"), score_id, cur)

    return score_id


db_file = sys.argv[2]
'''
try:
    os.remove(db_file)
except OSError:
    pass
'''
try:
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()
    cur.execute('PRAGMA foreign_keys=TRUE;')
    with open('scorelib.sql') as fp:
        cur.executescript(fp.read())
    
    for p in scorelib.load(sys.argv[1]):
        store_print(p, cur)
    
    conn.commit()
    
except Error as e:
    print(e)
finally:
    conn.close()










