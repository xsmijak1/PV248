import re


class Print:
    id_re = re.compile(r"Print Number: (\d+)\s*$")
    partiture_re = re.compile(r"Partiture: (.+)\s*$")

    def parse_print_id(str):
        id_match = re.match(Print.id_re, str)
        if id_match is not None and not id_match.group(1).isspace():
            return int(id_match.group(1))
        return 0

    def parse_partiture(str):
        match = re.match(Print.partiture_re, str)
        if match is not None:
            if "yes" in match.group(1):
                return True
            elif "no" in match.group(1):
                return False
        return None

    def partiture_to_str(partiture):
        if partiture:
            return "yes"
        return "no"

    def __init__(self, edition, print_id, partiture):
        self.edition = edition
        self.print_id = print_id
        self.partiture = partiture

    def format(self):
        print("Print Number: " + str(self.print_id))

        if len(self.composition().authors) > 0:
            formatted_composers = []
            for composer in self.composition().authors:
                composer_str = composer.name
                if composer.died is None and composer.born is None:
                    formatted_composers.append(composer_str)
                    continue
                composer_str += " ("
                if composer.born is not None:
                    composer_str += str(composer.born)
                composer_str += " -- "
                if composer.died is not None:
                    composer_str += str(composer.died)
                composer_str += ")"
                formatted_composers.append(composer_str)
            print("Composer: " + "; ".join(formatted_composers))
        else:
            print()

        if self.composition().name is not None:
            print("Title: " + self.composition().name)
        else:
            print()

        if self.composition().genre is not None:
            print("Genre: " + self.composition().genre)
        else:
            print()

        if self.composition().key is not None:
            print("Key: " + self.composition().key)
        else:
            print()

        if self.composition().year is not None:
            print("Composition Year: " + str(self.composition().year))
        else:
            print()

        if self.edition.name is not None:
            print("Edition: " + str(self.edition.name))
        else:
            print()

        if len(self.edition.authors) > 0:
            print("Editor: " + "; ".join(list(map(lambda editor: editor.name, self.edition.authors))))
        else:
            print()

        if any(self.composition().voices) and len(self.composition().voices) > 0:
            for i in range(len(self.composition().voices)):
                if self.composition().voices[i] is None:
                    print("Voice " + str(i + 1) + ": ")
                else:
                    print("Voice " + str(i + 1) + ": " + self.composition().voices[i].range_to_str() +
                          self.composition().voices[i].name)
        else:
            print()

        if self.partiture is not None:
            print("Partiture: " + Print.partiture_to_str(self.partiture))
        else:
            print()

        if self.composition().incipit is not None:
            print("Incipit: " + str(self.composition().incipit))
        else:
            print()

        print()

    def composition(self):
        return self.edition.composition


class Edition:
    name_re = re.compile(r"Edition: (.+)\s*$")

    def parse_name(str):
        match = re.match(Edition.name_re, str)
        if match is None or match.group(1).isspace():
            return None
        return match.group(1).strip()

    def __init__(self, composition, authors, name):
        self.composition = composition
        self.authors = authors
        self.name = name


class Composition:
    title_re = re.compile(r"Title: (.+)\s*$")
    genre_re = re.compile(r"Genre: (.+)\s*$")
    key_re = re.compile(r"Key: (.+)\s*$")
    year_re = re.compile(r".+ Year: (\d+)\s*$")
    incipit_re = re.compile(r"Incipit: (.+)\s*")

    def parse_title(str):
        match = re.match(Composition.title_re, str)
        if match is None or match.group(1).isspace():
            return None
        return match.group(1).strip()

    def parse_genre(str):
        match = re.match(Composition.genre_re, str)
        if match is None or match.group(1).isspace():
            return None
        return match.group(1).strip()

    def parse_key(str):
        match = re.match(Composition.key_re, str)
        if match is None or match.group(1).isspace():
            return None
        return match.group(1).strip()

    def parse_incipit(str):
        match = re.match(Composition.incipit_re, str)
        if match is None or match.group(1).isspace():
            return None
        return match.group(1).strip()

    def parse_year(str):
        match = re.match(Composition.year_re, str)
        if match is None or match.group(1).isspace():
            return None
        return int(match.group(1).strip())

    def __init__(self, name, incipit, key, genre, year, voices, authors):
        self.name = name
        self.incipit = incipit
        self.key = key
        self.genre = genre
        self.year = year
        self.voices = voices
        self.authors = authors


class Voice:
    voice_re = re.compile(r"Voice.*: (.+)\s*$")

    def parse_voice(str):
        match = re.match(Voice.voice_re, str)
        if match is None or match.group(1).isspace():
            return None
        parts = match.group(1).split(", ")
        if "--" not in parts[0]:
            return Voice(", ".join(parts).strip(), None)
        return Voice(", ".join(parts[1:]).strip(), parts[0].strip())

    def range_to_str(self):
        if self.range is None:
            return ""
        return self.range + ", "

    def __init__(self, name, range):
        self.name = name
        self.range = range


class Person:
    composers_re = re.compile(r"Composer: (.+)\s*$")
    editors_re = re.compile(r"Editor: (.+)\s*$")

    def parse_composers(str):
        match = re.match(Person.composers_re, str)
        if match is None or match.group(1).isspace():
            return []
        persons = []
        for person_str in match.group(1).split("; "):
            name = person_str.split(" (")[0]
            years = re.sub('--', '-', re.sub('[^\d-]', '', person_str)).split("-")
            if len(years) > 1 or years[0] != "":
                if len(years[0]) == 4:
                    born = int(years[0])
                else:
                    born = None
                if len(years) > 1 and len(years[1]) == 4:
                    died = int(years[1])
                elif len(years) == 3 and len(years[2]) == 4:
                    died = int(years[2])
                else:
                    died = None
            else:
                born = None
                died = None
            person = Person(name.strip(), born, died)
            persons.append(person)
        return persons

    def parse_editors(str):
        match = re.match(Person.editors_re, str)
        if match is None or match.group(1).isspace():
            return []
        persons = []
        for person_str in match.group(1).split("; "):
            if ", " in person_str:
                tokens = person_str.split(", ")
                for token in tokens:
                    words = token.split(" ")
                    if len(words) > 1:
                        persons.append(Person(token.strip(), None, None))
                    else:
                        persons.append(Person(person_str.strip(), None, None))
                        break
            else:
                persons.append(Person(person_str.strip(), None, None))
        return persons

    def __init__(self, name, born, died):
        self.name = name
        self.born = born
        self.died = died


def load(filename):
    prints = []
    f = open(filename, 'r')
    line = f.readline()
    while line:
        print_id = Print.parse_print_id(line)
        authors = Person.parse_composers(f.readline())
        composition_name = Composition.parse_title(f.readline())
        genre = Composition.parse_genre(f.readline())
        key = Composition.parse_key(f.readline())
        composition_year = Composition.parse_year(f.readline())
        line = f.readline()
        if "Publication" in line:
            line = f.readline()
        edition_name = Edition.parse_name(line)
        editors = Person.parse_editors(f.readline())
        line = f.readline()
        voices = []
        while "Voice" in line:
            voice = Voice.parse_voice(line)
            voices.append(voice)
            line = f.readline()
        partiture = Print.parse_partiture(line)
        if partiture is None:
            line = f.readline()
            partiture = Print.parse_partiture(line)
        incipit = Composition.parse_incipit(line)
        if incipit is None:
            incipit = Composition.parse_incipit(f.readline())
        composition = Composition(composition_name, incipit, key, genre, composition_year, voices, authors)
        edition = Edition(composition, editors, edition_name)
        p = Print(edition, print_id, partiture)
        prints.append(p)
        line = f.readline()
        while line and re.match(Print.id_re, line) is None:
            line = f.readline()

    return prints
